using System;
using System.Collections.Generic;

public static class Application
{
	public static void Main(String[] args)
	{
		double boxScale = 15;
		
		Scene.sceneObjects = new List<SceneObject>()
		{
			new SceneObject("Main Camera", new Vector3(-3.3, -8, 4), new Vector3(2.5, 6, -2).Normalized),

			new SceneObject("Lonely sphere 1", new Vector3(-2.2, -5.5, 3.2), new Vector3(0, 0, 0).Normalized),
			new SceneObject("Lonely sphere 2", new Vector3(1.5, 0, 1), new Vector3(0, 0, 0).Normalized),
			new SceneObject("Lonely sphere 3", new Vector3(-2.15, 2, 1), new Vector3(0, 0, 0).Normalized),
			new SceneObject("Lonely sphere 4", new Vector3(1, 5, 2), new Vector3(0, 0, 0).Normalized),

			new SceneObject("Horizontal Plane", new Vector3(0, 0, 0), Vector3.up),
			new SceneObject("Floor Plane", new Vector3(0, 0, boxScale), Vector3.down),
			new SceneObject("Front Plane", new Vector3(0, -boxScale, 0), Vector3.forward),
			new SceneObject("Back Plane", new Vector3(0, boxScale, 0), Vector3.backward),
			new SceneObject("Left Plane", new Vector3(-boxScale, 0, 0), Vector3.right),
			new SceneObject("Right Plane", new Vector3(boxScale, 0, 0), Vector3.left),

			new SceneObject("Point light 1", new Vector3(-5, 0, 3), new Vector3(0, 0, 0).Normalized),
			new SceneObject("Point light 2", new Vector3(5, 0, 3), new Vector3(0, 0, 0).Normalized),
			new SceneObject("Point light 3", new Vector3(0, 1, 6), new Vector3(0, 0, 0).Normalized)
		};

		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 1").AddComponent<Sphere>();
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 1").GetComponent<Material>().ReflectionStrength = 0.01;
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 1").GetComponent<Material>().RefractionStrength = 0.97;

		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 2").AddComponent<Sphere>();
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 2").GetComponent<Material>().ReflectionStrength = 0.3;
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 2").GetComponent<Material>().RefractionStrength = 0.2;

		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 3").AddComponent<Sphere>();
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 3").GetComponent<Material>().ReflectionStrength = 0.8;
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 3").GetComponent<Material>().RefractionStrength = 0.04;

		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 4").AddComponent<Sphere>();
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 4").GetComponent<Material>().ReflectionStrength = 0.3;
		Scene.sceneObjects.Find(x => x.name == "Lonely sphere 4").GetComponent<Material>().RefractionStrength = 0.4;


		Scene.sceneObjects.Find(x => x.name == "Horizontal Plane").AddComponent<Plane>();
		Scene.sceneObjects.Find(x => x.name == "Horizontal Plane").GetComponent<Material>().ReflectionStrength = 0.6;
		Scene.sceneObjects.Find(x => x.name == "Horizontal Plane").GetComponent<Material>().RefractionStrength = 0.01;

		Scene.sceneObjects.Find(x => x.name == "Floor Plane").AddComponent<Plane>();
		Scene.sceneObjects.Find(x => x.name == "Floor Plane").GetComponent<Material>().ReflectionStrength = 0.4;
		Scene.sceneObjects.Find(x => x.name == "Floor Plane").GetComponent<Material>().RefractionStrength = 0;

		Scene.sceneObjects.Find(x => x.name == "Front Plane").AddComponent<Plane>();
		Scene.sceneObjects.Find(x => x.name == "Front Plane").GetComponent<Material>().ReflectionStrength = 0.4;
		Scene.sceneObjects.Find(x => x.name == "Front Plane").GetComponent<Material>().RefractionStrength = 0;

		Scene.sceneObjects.Find(x => x.name == "Back Plane").AddComponent<Plane>();
		Scene.sceneObjects.Find(x => x.name == "Back Plane").GetComponent<Material>().ReflectionStrength = 0.4;
		Scene.sceneObjects.Find(x => x.name == "Back Plane").GetComponent<Material>().RefractionStrength = 0;

		Scene.sceneObjects.Find(x => x.name == "Left Plane").AddComponent<Plane>();
		Scene.sceneObjects.Find(x => x.name == "Left Plane").GetComponent<Material>().ReflectionStrength = 0.4;
		Scene.sceneObjects.Find(x => x.name == "Left Plane").GetComponent<Material>().RefractionStrength = 0;

		Scene.sceneObjects.Find(x => x.name == "Right Plane").AddComponent<Plane>();
		Scene.sceneObjects.Find(x => x.name == "Right Plane").GetComponent<Material>().ReflectionStrength = 0.4;
		Scene.sceneObjects.Find(x => x.name == "Right Plane").GetComponent<Material>().RefractionStrength = 0;



		Scene.sceneObjects.Find(x => x.name == "Point light 1").AddComponent<PointLight>().LightPower = 8;
		Scene.sceneObjects.Find(x => x.name == "Point light 2").AddComponent<PointLight>().LightPower = 8;
		Scene.sceneObjects.Find(x => x.name == "Point light 3").AddComponent<PointLight>().LightPower = 8;


		var camera = Scene.sceneObjects.Find(x => x.name == "Main Camera").AddComponent<Camera>();

		{
			camera.renderResolutionX = 1920;
			camera.renderResolutionY = 1080;
			camera.HorizontalFOV = 75;
		}

		camera.Render(args[0]);
	}
}
