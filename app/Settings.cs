using System.Drawing;

public static class Settings
{
	public static Color backgroundColor = Color.Black;
	public static double ambientLuminosity = 0.005;

	public static int maxBounces = 9;
}
