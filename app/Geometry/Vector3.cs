using System;

public struct Vector3 //Частично скопировано с DXSharp.Mathematics
{
	public double x, y, z;

#region Vector Constants

	public static readonly Vector3 zero = new Vector3();
	public static readonly Vector3 right = new Vector3(1, 0, 0);
	public static readonly Vector3 left = new Vector3(-1, 0, 0);
	public static readonly Vector3 forward = new Vector3(0, 1, 0);
	public static readonly Vector3 backward = new Vector3(0, -1, 0);
	public static readonly Vector3 up = new Vector3(0, 0, 1);
	public static readonly Vector3 down = new Vector3(0, 0, -1);
	public static readonly Vector3 NaN = new Vector3(Double.NaN, Double.NaN, Double.NaN);

#endregion

#region Constructors

	public Vector3(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3(double[] values)
	{
		if (values == null)
			throw new ArgumentNullException("values");
		if (values.Length != 3)
			throw new ArgumentOutOfRangeException("values", "There must be three and only three input values for Vector3.");

		x = values[0];
		y = values[1];
		z = values[2];
	}

#endregion


#region Operators

	public static Vector3 operator +(Vector3 value)
	{
		return value;
	}
	public static Vector3 operator -(Vector3 value)
	{
		return new Vector3(-value.x, -value.y, -value.z);
	}

	public static Vector3 operator +(Vector3 left, Vector3 right)
	{
		return new Vector3(left.x + right.x, left.y + right.y, left.z + right.z);
	}
	public static Vector3 operator -(Vector3 left, Vector3 right)
	{
		return left + (-right);
	}

	public static Vector3 operator *(Vector3 value, double scale)
	{
		return new Vector3(value.x * scale, value.y * scale, value.z * scale);
	}
	public static Vector3 operator *(double scale, Vector3 value)
	{
		return new Vector3(value.x * scale, value.y * scale, value.z * scale);
	}
	public static Vector3 operator /(Vector3 value, double scale)
	{
		return new Vector3(value.x / scale, value.y / scale, value.z / scale);
	}
	public static Vector3 operator /(double scale, Vector3 value)
	{
		return new Vector3(value.x / scale, value.y / scale, value.z / scale);
	}

#endregion


#region Properties

	public double SqrMagnitude
	{
		get
		{
			return x * x + y * y + z * z;
		}
	}

	public double Magnitude
	{
		get
		{
			return Math.Sqrt(this.SqrMagnitude);
		}
	}

	public Vector3 Normalized
	{
		get
		{
			double magnitude = this.Magnitude;

			if (magnitude != 0)
				return new Vector3(x / magnitude, y / magnitude, z / magnitude);
			else
				return Vector3.zero;
		}
	}

	public bool IsNormalized
	{
		get
		{
			return this.SqrMagnitude == 1;
		}
	}

	public override string ToString()
	{
		return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2}", x, y, z);
	}

#endregion


#region Static Functions

	public static Vector3 Parse(string str)
	{
		return new Vector3(Array.ConvertAll(str.Split(','), e => double.Parse(e, System.Globalization.CultureInfo.InvariantCulture.NumberFormat)));
	}

	public static double Distance(Vector3 vector1, Vector3 vector2)
	{
		return (vector1 - vector2).Magnitude;
	}

	public static double SqrDistance(Vector3 vector1, Vector3 vector2)
	{
		return (vector1 - vector2).SqrMagnitude;
	}

	public static double Dot(Vector3 vector1, Vector3 vector2)
	{
		return vector1.x * vector2.x +
		vector1.y * vector2.y +
		vector1.z * vector2.z;
	}

	public static double AngleCos(Vector3 vector1, Vector3 vector2)
	{
		return Vector3.Dot(vector1, vector2) / (vector1.Magnitude * vector2.Magnitude);
	}

	public static double Angle(Vector3 vector1, Vector3 vector2)
	{
		return Math.Acos(Vector3.Dot(vector1, vector2) / (vector1.Magnitude * vector2.Magnitude));
	}

	public static Vector3 Cross(Vector3 vector1, Vector3 vector2)
	{
		return new Vector3(
		vector1.y * vector2.z - vector1.z * vector2.y,
		vector1.z * vector2.x - vector1.x * vector2.z,
		vector1.x * vector2.y - vector1.y * vector2.x
		);
	}

	public static Vector3 Reflect(Vector3 vector, Vector3 normal)
	{
		return vector - 2 * Vector3.Dot(vector, normal) * normal;
	}

	public static Vector3 Refract(Vector3 vector, Vector3 normal, double index1, double index2)
	{
		double vecNormDot = (Dot(vector, normal));
		return vector +
		(Math.Sqrt((index2 * index2 - index1 * index1) / (vecNormDot * vecNormDot) + 1) - 1) * vecNormDot * normal;
	}
#endregion

}
