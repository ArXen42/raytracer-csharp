using System;

public static class LightingModels
{
	public readonly static Material.LightingModelDelegate lambert = delegate(VisibleObject visibleObject, Vector3 position, Vector3 viewPosition, PointLight light)
	{
		Ray toLightRay = Ray.GetFromToRay(position, light.transform.position);
		if (!toLightRay.IsCrossesAnything(Vector3.Distance(position, light.transform.position)))
		{
			double angleCos = Vector3.Dot(visibleObject.GetNormalAt(position), toLightRay.direction);

			double deltaLuminosity = light.LightPower * angleCos / Vector3.SqrDistance(position, light.transform.position);
			return Math.Max(0, deltaLuminosity);
		}
		else
			return 0;
	};
}