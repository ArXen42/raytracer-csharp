public class Component
{
	public Component(SceneObject ownerSceneObject)
	{
		this.ownerSceneObject = ownerSceneObject;
	}
	public readonly SceneObject ownerSceneObject;

	#region Helpers

	public Transform transform
	{
		get
		{
			return this.ownerSceneObject.GetComponent<Transform>();
		}
	}

	#endregion
}