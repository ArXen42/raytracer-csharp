using System;
using System.Collections.Generic;
using System.Drawing;

public struct Ray
{
	public Vector3 start, direction;
	public SceneObject excludeSceneObject;

	public Ray(Vector3 from, Vector3 direction)
	{
		this.start = from;
		this.direction = direction;
		excludeSceneObject = null;
	}
	public Ray(Vector3 from, Vector3 direction, SceneObject excludeSceneObject)
	{
		this.start = from;
		this.direction = direction;
		this.excludeSceneObject = excludeSceneObject;
	}

	public bool IsCrossesAnything(double dist)
	{
		foreach (var sceneObject in Scene.sceneObjects)
		{
			var visibleComponent = sceneObject.GetComponent<VisibleObject>();

			if (visibleComponent != null)
			if (visibleComponent.IsRayCrosses(this))
			{
				if (Vector3.Distance(visibleComponent.GetRayCross(this), this.start) < dist)
					return true;
			}
		}

		return false;
	}


	public static Ray GetFromToRay(Vector3 from, Vector3 to)
	{
		return new Ray(from, (to - from).Normalized);
	}

	public static Color TraceRay(Ray ray, int count = 0)
	{
		if (count < Settings.maxBounces)
		{
			Vector3 nearestRayCross = Vector3.zero;
			double lastNearestDist = Double.MaxValue;

			VisibleObject nearestVisibleComponent = null;

			foreach (var sceneObject in Scene.sceneObjects)
			{
				var visibleComponent = sceneObject.GetComponent<VisibleObject>();

				if (sceneObject != ray.excludeSceneObject && visibleComponent != null)
				if (visibleComponent.IsRayCrosses(ray))
				{
					Vector3 crossPos = visibleComponent.GetRayCross(ray);
					double dist = Vector3.Distance(ray.start, crossPos);
					if (dist < lastNearestDist)
					{
						nearestRayCross = crossPos;
						nearestVisibleComponent = visibleComponent;
						lastNearestDist = dist;
					}
				}
			}

			if (nearestVisibleComponent != null)
			{
				var material = nearestVisibleComponent.ownerSceneObject.GetComponent<Material>();

				double thisPointStrength = 1 - material.ReflectionStrength - material.RefractionStrength;
				Color thisPointColor = nearestVisibleComponent.GetLightedColorAt(nearestRayCross, ray.start);

				var reflectColor = Ray.TraceRay(new Ray(nearestRayCross,
				Vector3.Reflect(ray.direction, nearestVisibleComponent.GetNormalAt(nearestRayCross)),
				nearestVisibleComponent.ownerSceneObject),
				count + 1);

				var refractColor = Ray.TraceRay(new Ray(nearestRayCross,
				Vector3.Refract(ray.direction, nearestVisibleComponent.GetNormalAt(nearestRayCross), 0, material.RefractiveIndex),
				nearestVisibleComponent.ownerSceneObject),
				count + 1);

				return Color.FromArgb((int)(thisPointStrength * thisPointColor.R + material.ReflectionStrength * reflectColor.R + material.RefractionStrength * refractColor.R),
				(int)(thisPointStrength * thisPointColor.G + material.ReflectionStrength * reflectColor.G + material.RefractionStrength * refractColor.G),
				(int)(thisPointStrength * thisPointColor.B + material.ReflectionStrength * reflectColor.B + material.RefractionStrength * refractColor.B));
			}
		}

		return Settings.backgroundColor;
	}
}
