using System.Collections.Generic;
using System.Drawing;

public class SceneObject
{
	public string name;
	public List<Component> components; //List of all components, attached to this SceneObject

#region Components

	public ComponentType GetComponent<ComponentType>() where ComponentType:Component
	{
		return components.Find(component => component is ComponentType) as ComponentType;
	}

	public List<ComponentType> GetComponents<ComponentType>() where ComponentType:Component
	{
		return components.FindAll(component => component is ComponentType).ConvertAll<ComponentType>(comp => comp as ComponentType);
	}

	public ComponentType AddComponent<ComponentType>() where ComponentType:Component
	{
		var constructor = typeof(ComponentType).GetConstructor(new [] {typeof(SceneObject)});
		var createdComponent = constructor.Invoke(new object[] {this}) as ComponentType;

		this.components.Add(createdComponent);
		return createdComponent;
	}

#endregion

#region Constructors

	public SceneObject(string name, Vector3 position, Vector3 direction)
	{
		this.name = name;
		components = new List<Component>();

		var transform = this.AddComponent<Transform>();
		transform.position = position;
		transform.direction = direction;

		this.AddComponent<Material>();
	}

#endregion

#region Helpers

		public Transform transform
		{
			get
			{
				return this.GetComponent<Transform>();
			}
		}

#endregion
}
