public class PointLight : Component
{
	public PointLight(SceneObject ownerSceneObject) : base(ownerSceneObject)
	{
		this.lightPower = 1;
	}

	private double lightPower;
	public double LightPower
	{
		get
		{
			return lightPower;
		}
		set
		{
			if (value >= 0)
				lightPower = value;
		}
	}
}
