using System;

public class Material : Component
{
	public Material(SceneObject ownerSceneObject) : base(ownerSceneObject)
	{
		this.lightingModel = LightingModels.lambert;
	}

	public delegate double LightingModelDelegate(VisibleObject visibleObject, Vector3 position, Vector3 viewPosition, PointLight light);

	private LightingModelDelegate lightingModel;
	public LightingModelDelegate LightingModel
	{
		get
		{
			return lightingModel;
		}
		set
		{
			if (value != null) lightingModel = value;
		}
	}


	public double ReflectionStrength
	{ get; set; } = 0.5;

	public double RefractiveIndex
	{ get; set; } = 0.3;
	public double RefractionStrength
	{ get; set; } = 0.3;
}