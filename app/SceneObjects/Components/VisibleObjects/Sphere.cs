using System;
using System.Drawing;

public class Sphere : VisibleObject
{
	public Sphere(SceneObject ownerSceneObject) : base(ownerSceneObject)
	{
	}

	private double radius = 1;
	public double Radius
	{
		get
		{
			return radius;
		}
		set
		{
			radius = value > 0 ? value : 0;
		}
	}

	public Color SelfColor
	{ get; set; } = Color.LightYellow;

	public override Vector3 GetNormalAt(Vector3 point)
	{
		return (point - this.transform.position).Normalized;
	}

	public override Color GetColorAt(Vector3 point)
	{
		return this.SelfColor;
	}

	public override bool IsRayCrosses(Ray ray)
	{
		double angleCos = Vector3.AngleCos(ray.direction, this.transform.position - ray.start);
		double sqrAngleCos = angleCos * angleCos;
		if (angleCos < 0)
			return false;

		double sqrDistanceBetweenStartAndSphereCenter = Vector3.SqrDistance(ray.start, this.transform.position);
		double sqrDistanceBetweenRayAndSphereCenter = (1 - sqrAngleCos) * sqrDistanceBetweenStartAndSphereCenter / sqrAngleCos;

		return sqrDistanceBetweenRayAndSphereCenter < radius * radius;
	}

	public override Vector3 GetRayCross(Ray ray)
	{
		double angleCos = Vector3.AngleCos(ray.direction, this.transform.position - ray.start);
		double sqrAngleCos = angleCos * angleCos;

		double sqrDistanceBetweenStartAndSphereCenter = Vector3.SqrDistance(ray.start, this.transform.position);
		double distanceBetweenStartAndSphereCenter = Vector3.Distance(ray.start, this.transform.position);

		double sqrDistanceBetweenRayAndSphereCenter = (1 - sqrAngleCos) * sqrDistanceBetweenStartAndSphereCenter / sqrAngleCos;

		if (sqrDistanceBetweenRayAndSphereCenter > radius * radius)
			return Vector3.NaN;
		else
			return ray.start + ray.direction * ((distanceBetweenStartAndSphereCenter / angleCos) - Math.Sqrt(radius * radius - sqrDistanceBetweenRayAndSphereCenter));
	}
};












