using System.Drawing;

public class Plane : VisibleObject
{
	public Plane(SceneObject ownerSceneObject) : base(ownerSceneObject)
	{
	}

	public override Vector3 GetNormalAt(Vector3 point)
	{
		return this.transform.direction;
	}

	public override System.Drawing.Color GetColorAt(Vector3 point)
	{
		return Color.Azure;
	}

	public override bool IsRayCrosses(Ray ray)
	{
		return Vector3.Dot(ray.direction, transform.direction) < 0;
	}

	public override Vector3 GetRayCross(Ray ray)
	{
		double dot = Vector3.Dot(ray.direction, transform.direction);
		if (dot > 0)
			return Vector3.NaN;
		else
		{
			double dist = -Vector3.Dot(ray.start - transform.position, transform.direction) / dot;
			return ray.start + ray.direction * dist;
		}
	}

	public new System.Drawing.Color GetLightedColorAt(Vector3 position, Vector3 viewPosition)
	{
		return base.GetLightedColorAt(position, viewPosition);
	}
}
