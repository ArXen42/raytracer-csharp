using System;
using System.Collections.Generic;
using System.Drawing;

public abstract class VisibleObject : Component
{
	public VisibleObject(SceneObject ownerSceneObject) : base(ownerSceneObject)
	{
	}

	public abstract Vector3 GetNormalAt(Vector3 point); //Returns normal at point
	public abstract Color GetColorAt(Vector3 point);  //Returns color at point or traces next ray(s)
	public abstract bool IsRayCrosses(Ray ray);  //If ray crosses this object returns true
	public abstract Vector3 GetRayCross(Ray ray); //Returns ray cross position or zero if there is no cross

	public Color GetLightedColorAt(Vector3 position, Vector3 viewPosition)
	{
		double luminosity = Settings.ambientLuminosity;

		var material = ownerSceneObject.GetComponent<Material>();

		foreach (var light in Scene.sceneObjects.FindAll(obj => obj.GetComponent<PointLight>() != null).ConvertAll<PointLight>(obj => obj.GetComponent<PointLight>()))
			luminosity += material.LightingModel(this, position, viewPosition, light);

		if (luminosity > 1)
			luminosity = 1;

		var color = GetColorAt(position);

		return Color.FromArgb((byte)(luminosity * color.R),
		(byte)(luminosity * color.G),
		(byte)(luminosity * color.B));
	}
}