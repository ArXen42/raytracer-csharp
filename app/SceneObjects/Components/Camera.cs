using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.NetworkInformation.MacOsStructs;
using System.Timers;

public class Camera : Component
{
	public Camera(SceneObject ownerSceneObject) : base(ownerSceneObject)
	{
	}

	public int renderResolutionX;
	public int renderResolutionY;


	private double horizontalFOV;
	public double HorizontalFOV
	{
		get
		{
			return horizontalFOV * 180 / Math.PI;
		}
		set
		{
			horizontalFOV = value * Math.PI / 180;
		}
	}

	public void Render(string pathToSave)
	{
		DateTime startTime = DateTime.Now;

		Bitmap image = new Bitmap(renderResolutionX, renderResolutionY);

		double ySide = renderResolutionX / (2 * Math.Tan(horizontalFOV / 2));

		Vector3 directionNormal = Vector3.Cross(Vector3.forward, transform.direction).Normalized; //Будем поворачивать каждый луч вокруг этой нормали

		double cos = Vector3.AngleCos(transform.direction, Vector3.forward); //На этот угол;
		double sin = Math.Sqrt(1 - cos * cos);


		double rotX = directionNormal.x, rotY = directionNormal.y, rotZ = directionNormal.z;

		var m11 = cos + (1 - cos) * rotX * rotX;
		var m12 = (1 - cos) * rotX * rotY - sin * rotZ;
		var m13 = (1 - cos) * rotX * rotZ + sin * rotY;

		var m21 = (1 - cos) * rotY * rotX + sin * rotZ;
		var m22 = cos + (1 - cos) * rotY * rotY;
		var m23 = (1 - cos) * rotY * rotZ - sin * rotX;

		var m31 = (1 - cos) * rotZ * rotX - sin * rotY;
		var m32 = (1 - cos) * rotZ * rotY + sin * rotX;
		var m33 = cos + (1 - cos) * rotZ * rotZ;

		int counter = 0, count = renderResolutionX * renderResolutionY;

		for (int x = 0; x < renderResolutionX; x++)
			for (int y = 0; y < renderResolutionY; y++)
			{
				counter++;
				if (counter % (count / 100) == 0)
				{
					Console.CursorLeft = 0;
					Console.Write("{0}%", counter * 100 / count);
				}

				double xSide = x - renderResolutionX / 2;
				double zSide = renderResolutionY / 2 - y;
				Vector3 dir = ((new Vector3(xSide, ySide, zSide))).Normalized;
				dir = new Vector3(
				m11 * dir.x + m12 * dir.y + m13 * dir.z,
				m21 * dir.x + m22 * dir.y + m23 * dir.z,
				m31 * dir.x + m32 * dir.y + m33 * dir.z
				);

				image.SetPixel(x, y, Ray.TraceRay(new Ray(transform.position, dir)));
			}

		image.Save(pathToSave, ImageFormat.Png);

		Console.WriteLine("Рендер в файл {0} завершен, время рендера: {1}", pathToSave, DateTime.Now - startTime);
	}
}
